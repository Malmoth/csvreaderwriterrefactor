﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using AddressProcessing.CSV.Contracts;

namespace AddressProcessing.CSV
{
    public sealed class CSVWriter : ICSVWriter
    {
        private StreamWriter _streamWriter;

        public void CreateOrResetFile(string fileName)
        {
            _streamWriter?.Close();
            var fileInfo = new FileInfo(fileName);
            _streamWriter = fileInfo.CreateText();
        }

        public void OpenFile(string fileName)
        {
            _streamWriter?.Close();
            var fileInfo = new FileInfo(fileName);
            _streamWriter = fileInfo.AppendText();
        }

        public void WriteColumns(IEnumerable<string> columns)
        {
            var columnsToWrite = columns.ToList();
            var outputBuilder = new StringBuilder();

            var totalColumnCount = columnsToWrite.Count;
            var totalSeparatorCount = totalColumnCount - 1;

            for (int i = 0; i < columnsToWrite.Count; i++)
            {
                outputBuilder.Append(columnsToWrite[i]);
                if (i != totalSeparatorCount)
                {
                    outputBuilder.Append(Constants.TabSeparator);
                }
            }

            var output = outputBuilder.ToString();
            _streamWriter.WriteLine(output);
        }

        public void WriteFile(IEnumerable<IEnumerable<string>> content)
        {
            if (content == null)
            {
                throw new ArgumentNullException(nameof(content));
            }

            foreach (var line in content)
            {
                WriteColumns(line);
            }
        }

        public void Close()
        {
            _streamWriter?.Close();
            _streamWriter = null;
        }

        private void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    _streamWriter?.Close();
                    _streamWriter = null;
                }
            }
            finally
            {
            }
        }

        public void Dispose()
        {
            Dispose(true);

            // Not necessary since the class is sealed, but don't forget to uncomment it and add a destructor if the sealed keyword is removed at some point.
            //GC.SuppressFinalize(this);
        }
    }
}