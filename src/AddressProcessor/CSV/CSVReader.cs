﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using AddressProcessing.CSV.Contracts;

namespace AddressProcessing.CSV
{
    public sealed class CSVReader : ICSVReader
    {
        private static readonly char[] LineSeparators = { Constants.NewLineSeparator };
        private static readonly char[] ColumnSeparators = { Constants.TabSeparator };
        private StreamReader _streamReader;

        public void OpenFile(string fileName)
        {
            _streamReader?.Close();
            _streamReader = File.OpenText(fileName);
        }

        public IEnumerable<string> ReadColumns()
        {
            var line = _streamReader.ReadLine();
            var columns = line?.Split(ColumnSeparators);
            return columns;
        }

        public IEnumerable<IEnumerable<string>> ReadFile()
        {
            var content = _streamReader.ReadToEnd();
            var lines = content?.Split(LineSeparators, StringSplitOptions.RemoveEmptyEntries);
            var result = lines?.Select(l => l.Split(ColumnSeparators));
            return result;
        }

        public void Close()
        {
            _streamReader?.Close();
            _streamReader = null;
        }

        private void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    _streamReader?.Close();
                    _streamReader = null;
                }
            }
            finally
            {
            }
        }

        public void Dispose()
        {
            Dispose(true);

            // Not necessary since the class is sealed, but don't forget to uncomment it and add a destructor if the sealed keyword is removed at some point.
            //GC.SuppressFinalize(this);
        }
    }
}