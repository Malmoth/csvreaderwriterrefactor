﻿using System;
using System.Collections.Generic;

namespace AddressProcessing.CSV.Contracts
{
    public interface ICSVWriter : IDisposable
    {
        void CreateOrResetFile(string fileName);
        void OpenFile(string fileName);
        void WriteColumns(IEnumerable<string> columns);
        void WriteFile(IEnumerable<IEnumerable<string>> content);
        void Close();
    }
}