﻿using System;
using System.Collections.Generic;

namespace AddressProcessing.CSV.Contracts
{
    public interface ICSVReader : IDisposable
    {
        void OpenFile(string fileName);
        IEnumerable<string> ReadColumns();
        IEnumerable<IEnumerable<string>> ReadFile();
        void Close();
    }
}