﻿namespace AddressProcessing.CSV
{
    internal struct Constants
    {
        internal const char TabSeparator = '\t';
        internal const char NewLineSeparator = '\n';
    }
}