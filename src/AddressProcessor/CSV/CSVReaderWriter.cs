﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using AddressProcessing.CSV.Contracts;

namespace AddressProcessing.CSV
{
    /*
        2) Refactor this class into clean, elegant, rock-solid & well performing code, without over-engineering.
           Assume this code is in production and backwards compatibility must be maintained.
    */


    /// <summary>
    /// Assuming backwards compatibility requirement means a drop-in replacement, meaning true full backwards compatibility.
    /// However, this is obviously a bad idea since this whole class shouldn't even exist in its current state.
    /// It merits a true refactor along with all the applications that consume it in any way.
    /// Nevertheless, attempting to do the pseudo-refactor as per the apparent requirements of this code-test.
    /// </summary>
    [Obsolete("Replace with CSVReader and CSVWriter classes wherever possible.")]
    public class CSVReaderWriter : IDisposable
    {
        private const int FirstColumnIndex = 0;
        private const int SecondColumnIndex = 1;
        private readonly ICSVReader _csvReader;
        private readonly ICSVWriter _csvWriter;

        [Flags]
        public enum Mode { Read = 1, Write = 2 };

        public CSVReaderWriter()
        {
            _csvReader = new CSVReader();
            _csvWriter = new CSVWriter();
        }

        public void Open(string fileName, Mode mode)
        {
            if (mode == Mode.Read)
            {
                _csvReader.OpenFile(fileName);
            }
            else if (mode == Mode.Write)
            {
                _csvWriter.CreateOrResetFile(fileName);
            }
            else
            {
                throw new ArgumentException($"Unknown file mode for {fileName}", nameof(mode));
            }
        }

        public void Write(params string[] columns)
        {
            _csvWriter.WriteColumns(columns);
        }

        [Obsolete("This method is broken, and it will never return a result. Please use the Read method with out parameters instead.")]
        public bool Read(string column1, string column2)
        {
            return Read(out column1, out column2);
        }

        public bool Read(out string column1, out string column2)
        {
            IReadOnlyList<string> columns = _csvReader.ReadColumns()?.ToList();

            if (columns == null || columns.Count == 0)
            {
                column1 = null;
                column2 = null;

                return false;
            }
            else
            {
                column1 = columns[FirstColumnIndex];
                column2 = columns[SecondColumnIndex];

                return true;
            }
        }

        public void Close()
        {
            _csvReader?.Close();
            _csvWriter?.Close();
        }

        public void Dispose()
        {
            _csvReader?.Dispose();
            _csvWriter?.Dispose();
        }
    }
}
