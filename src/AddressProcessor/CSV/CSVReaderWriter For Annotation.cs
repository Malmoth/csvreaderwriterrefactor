﻿using System;
using System.IO;

namespace AddressProcessing.CSV
{
    /*
        1) List three to five key concerns with this implementation that you would discuss with the junior developer. 

        Please leave the rest of this file as it is so we can discuss your concerns during the next stage of the interview process.
        
        1) With the current design, this class is holding state. The reader and writer streams represent a state from the application perspective.
           Depending on the archirecture of the application that consumes this class, that may not be the ideal way of handling this operation.
        2) Having separate reader and writer streams in the same class and performing both operations in it is definitely a bad practice.
           Ideally there should be separate classes for each operation: A CsvReader and a CsvWriter.
           The current situation is beyond a simple breach of the single responsibility principle.
        3) The reader and writer streams are prone to memory leaks. This is partly owing to the design of the class, but not just that.
           The lack of proper exception handling and IDisposable implementation would eventually cause leaks.
           Since the current design results in us not being able to simply rely on using statements, we'll need to handle edge cases manually.
        4) The null checks in the Close method are not really thread-safe. This is a trap most junior developers fall for.
        5) There is a redundant Read method on the class that will never return anything since the parameters are not marked as out.
           It's obvious that it was kept for backwards-compatibility, but even in that case it should throw an explicit exception or at the very least be marked as Obsolete.
        6) Any method that returns a boolean as a success/error result, and expose tha actual data as an out parameter should be prefixed with Try in line with .NET naming conventions.
           This will make it easier for whoever consumes that method to realise how exactly that method works.
        7) A Read method that returns specifically 2 columns seems a bit of a code-smell, but in the context of tha particular application it could possibly be an actual requirement.
           Even in that case, it should've been named properly.
        8) Marking the Mode enum with FlagsAttribute, we're basically telling whoever consumes this class that this particular class can handle both read and write
           operations on the very same file. Which it clearly can not, and obviously should not.
        9) We should avoid throwing an instance of the base Exception class. Exceptions need to be meaningful not merely by their messages, but by their types as well.
    */

    public class CSVReaderWriterForAnnotation
    {
        private StreamReader _readerStream = null;
        private StreamWriter _writerStream = null;

        [Flags]
        public enum Mode { Read = 1, Write = 2 };

        public void Open(string fileName, Mode mode)
        {
            if (mode == Mode.Read)
            {
                _readerStream = File.OpenText(fileName);
            }
            else if (mode == Mode.Write)
            {
                FileInfo fileInfo = new FileInfo(fileName);
                _writerStream = fileInfo.CreateText();
            }
            else
            {
                throw new Exception("Unknown file mode for " + fileName);
            }
        }

        public void Write(params string[] columns)
        {
            string outPut = "";

            for (int i = 0; i < columns.Length; i++)
            {
                outPut += columns[i];
                if ((columns.Length - 1) != i)
                {
                    outPut += "\t";
                }
            }

            WriteLine(outPut);
        }

        public bool Read(string column1, string column2)
        {
            const int FIRST_COLUMN = 0;
            const int SECOND_COLUMN = 1;

            string line;
            string[] columns;

            char[] separator = { '\t' };

            line = ReadLine();
            columns = line.Split(separator);

            if (columns.Length == 0)
            {
                column1 = null;
                column2 = null;

                return false;
            }
            else
            {
                column1 = columns[FIRST_COLUMN];
                column2 = columns[SECOND_COLUMN];

                return true;
            }
        }

        public bool Read(out string column1, out string column2)
        {
            const int FIRST_COLUMN = 0;
            const int SECOND_COLUMN = 1;

            string line;
            string[] columns;

            char[] separator = { '\t' };

            line = ReadLine();

            if (line == null)
            {
                column1 = null;
                column2 = null;

                return false;
            }

            columns = line.Split(separator);

            if (columns.Length == 0)
            {
                column1 = null;
                column2 = null;

                return false;
            }
            else
            {
                column1 = columns[FIRST_COLUMN];
                column2 = columns[SECOND_COLUMN];

                return true;
            }
        }

        private void WriteLine(string line)
        {
            _writerStream.WriteLine(line);
        }

        private string ReadLine()
        {
            return _readerStream.ReadLine();
        }

        public void Close()
        {
            if (_writerStream != null)
            {
                _writerStream.Close();
            }

            if (_readerStream != null)
            {
                _readerStream.Close();
            }
        }
    }
}
