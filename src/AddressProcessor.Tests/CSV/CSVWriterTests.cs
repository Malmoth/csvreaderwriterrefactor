﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using AddressProcessing.CSV;
using NUnit.Framework;
using FileOperationMode = AddressProcessing.CSV.CSVReaderWriter.Mode;

namespace Csv.Tests
{
    [TestFixture]
    //[NonParallelizable]
    public class CSVrWriterTests
    {
        private CSVWriter _sut;
        private Guid _fileId;
        private string _currentDirectoryPath;

        [SetUp]
        public void Initialize()
        {
            _sut = new CSVWriter();
            _fileId = Guid.NewGuid();
            _currentDirectoryPath = TestContext.CurrentContext.TestDirectory;
        }

        #region Open
        
        [Test]
        public void OpenFile_FileNameNull_ShouldThrowException()
        {
            try
            {
                _sut.OpenFile(null);
                Assert.Fail("It should've thrown an exception.");
            }
            catch (Exception ex)
            {
                Assert.IsNotNull(ex);
                Assert.IsInstanceOf<ArgumentNullException>(ex);
            }
        }

        [Test]
        public void OpenFile_FileNameStringEmpty_ShouldThrowException()
        {
            try
            {
                _sut.OpenFile(string.Empty);
                Assert.Fail("It should've thrown an exception.");
            }
            catch (Exception ex)
            {
                Assert.IsNotNull(ex);
                Assert.IsInstanceOf<ArgumentException>(ex);
            }
        }

        [Test]
        public void OpenFile_FileAlreadyExist_ShouldNotThrowException()
        {
            try
            {
                var filePath = $"{_currentDirectoryPath}\\test_data\\{_fileId}.csv";
                _sut.OpenFile(filePath);
                _sut.WriteColumns(new []{"Test 1", "Test 2"});
                _sut.Close();

                _sut.OpenFile(filePath);
                _sut.WriteColumns(new[] { "Test 3", "Test 4" });
                _sut.Close();
            }
            catch (Exception)
            {
                Assert.Fail("It should not have thrown an exception.");
            }
        }

        #endregion

        #region Close

        [Test]
        public void Close_OnNonOpenedFile_ShouldNotThrowException()
        {
            try
            {
                _sut.Close();
            }
            catch (Exception)
            {
                Assert.Fail("It should not have thrown an exception.");
            }
        }

        #endregion

        #region Write

        [Test]
        public void Write_FileDoesntExist_ShouldCreateFileAndWrite()
        {
            try
            {
                var filePath = $"{_currentDirectoryPath}\\test_data\\{_fileId}.csv";
                _sut.OpenFile(filePath);

                _sut.WriteColumns(new []{"test 0", "test 1"});
            }
            catch (Exception)
            {
                Assert.Fail("It should not have thrown an exception.");
            }
        }

        [Test]
        public void Write_MultipleEntries_ShouldSaveAll()
        {
            try
            {
                var filePath = $"{_currentDirectoryPath}\\test_data\\{_fileId}.csv";

                _sut.OpenFile(filePath);
                _sut.WriteColumns(new[] { "test 0", "test 1" });
                _sut.WriteColumns(new [] {"test 2", "test 3"});
                _sut.WriteColumns(new [] {"test 4", "test 5"});
                _sut.WriteColumns(new [] {"test 6", "test 7"});
                _sut.Close();
            }
            catch (Exception)
            {
                Assert.Fail("It should not have thrown an exception.");
            }
        }

        #endregion

        #region WriteFile

        [Test]
        public void WriteFile_MultipleEntries_ShouldSaveAll()
        {
            try
            {
                var filePath = $"{_currentDirectoryPath}\\test_data\\{_fileId}.csv";

                var content = new List<IEnumerable<string>>
                {
                    new[] {"test 0", "test 1"},
                    new[] {"test 2", "test 3"},
                    new[] {"test 4", "test 5"},
                    new[] {"test 6", "test 7"},
                };
                _sut.OpenFile(filePath);
                _sut.WriteFile(content);
                _sut.Close();
            }
            catch (Exception)
            {
                Assert.Fail("It should not have thrown an exception.");
            }
        }

        #endregion
    }
}
