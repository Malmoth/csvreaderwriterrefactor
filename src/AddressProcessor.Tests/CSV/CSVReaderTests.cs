﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using AddressProcessing.CSV;
using NUnit.Framework;
using FileOperationMode = AddressProcessing.CSV.CSVReaderWriter.Mode;

namespace Csv.Tests
{
    [TestFixture]
    //[NonParallelizable]
    public class CSVReaderTests
    {
        private CSVReader _sut;
        private Guid _fileId;
        private string _currentDirectoryPath;

        [SetUp]
        public void Initialize()
        {
            _sut = new CSVReader();
            _fileId = Guid.NewGuid();
            _currentDirectoryPath = TestContext.CurrentContext.TestDirectory;
        }

        #region Open

        [Test]
        public void OpenFile_FileNameNull_ShouldThrowException()
        {
            try
            {
                _sut.OpenFile(null);
                Assert.Fail("It should've thrown an exception.");
            }
            catch (Exception ex)
            {
                Assert.IsNotNull(ex);
                Assert.IsInstanceOf<ArgumentNullException>(ex);
            }
        }

        [Test]
        public void OpenFile_FileNameStringEmpty_ShouldThrowException()
        {
            try
            {
                _sut.OpenFile(string.Empty);
                Assert.Fail("It should've thrown an exception.");
            }
            catch (Exception ex)
            {
                Assert.IsNotNull(ex);
                Assert.IsInstanceOf<ArgumentException>(ex);
            }
        }

        [Test]
        public void OpenFile_FileDoesNotExist_ShouldThrowException()
        {
            try
            {
                _sut.OpenFile($"{_currentDirectoryPath}\\does_not_exist.csv");
                Assert.Fail("It should've thrown an exception.");
            }
            catch (Exception ex)
            {
                Assert.IsNotNull(ex);
                Assert.IsInstanceOf<FileNotFoundException>(ex);
            }
        }

        [Test]
        public void OpenFile_FileInWrongLocation_ShouldThrowException()
        {
            try
            {
                _sut.OpenFile($"{_currentDirectoryPath}\\does_not_exist\\contacts.csv");
                Assert.Fail("It should've thrown an exception.");
            }
            catch (Exception ex)
            {
                Assert.IsNotNull(ex);
                Assert.IsInstanceOf<DirectoryNotFoundException>(ex);
            }
        }

        [Test]
        public void OpenFile_FileAlreadyExist_ShouldNotThrowException()
        {
            try
            {
                var filePath = $"{_currentDirectoryPath}\\test_data\\contacts.csv";
                _sut.OpenFile(filePath);
            }
            catch (Exception)
            {
                Assert.Fail("It should not have thrown an exception.");
            }
        }

        #endregion

        #region Close

        [Test]
        public void Close_OnNonOpenedFile_ShouldNotThrowException()
        {
            try
            {
                _sut.Close();
            }
            catch (Exception)
            {
                Assert.Fail("It should not have thrown an exception.");
            }
        }

        #endregion

        #region Read

        [Test]
        public void Read_FileExist_ShouldReturnValues()
        {
            try
            {
                var filePath = $"{_currentDirectoryPath}\\test_data\\contacts.csv";
                _sut.OpenFile(filePath);

                var result = _sut.ReadColumns();

                Assert.IsNotNull(result);
                Assert.AreEqual(4, result.Count());
            }
            catch (Exception)
            {
                Assert.Fail("It should not have thrown an exception.");
            }
        }

        [Test]
        public void Read_FileExist_ShouldReturnConsecutiveValues()
        {
            try
            {
                var filePath = $"{_currentDirectoryPath}\\test_data\\contacts.csv";
                _sut.OpenFile(filePath);

                var row1 = _sut.ReadColumns().ToArray();
                var row2 = _sut.ReadColumns().ToArray();

                Assert.AreEqual(row1[0], "Shelby Macias");
                Assert.AreEqual(row2[0], "Porter Coffey");
            }
            catch (Exception)
            {
                Assert.Fail("It should not have thrown an exception.");
            }
        }

        #endregion

        #region ReadFile

        [Test]
        public void ReadFile_FileExist_ShouldReturnAllRows()
        {
            try
            {
                var filePath = $"{_currentDirectoryPath}\\test_data\\contacts.csv";
                _sut.OpenFile(filePath);

                var content = _sut.ReadFile().ToArray();

                Assert.AreEqual(229, content.Length);
            }
            catch (Exception)
            {
                Assert.Fail("It should not have thrown an exception.");
            }
        }

        [Test]
        public void ReadFile_FileExist_ReturnValuesShouldBeCorrect()
        {
            try
            {
                var filePath = $"{_currentDirectoryPath}\\test_data\\contacts.csv";
                _sut.OpenFile(filePath);

                var content = _sut.ReadFile().ToArray();

                Assert.AreEqual("Shelby Macias", content.First().First());
                Assert.AreEqual("Leila Neal", content.Last().First());
            }
            catch (Exception)
            {
                Assert.Fail("It should not have thrown an exception.");
            }
        }

        #endregion

    }
}
