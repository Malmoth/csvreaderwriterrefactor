﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using AddressProcessing.CSV;
using NUnit.Framework;
using FileOperationMode = AddressProcessing.CSV.CSVReaderWriterForAnnotation.Mode;

namespace Csv.Tests
{
    [TestFixture]
    public class OriginalCSVReaderWriterTests
    {
        private CSVReaderWriterForAnnotation _sut;
        private Guid _fileId;
        private string _currentDirectoryPath;

        [SetUp]
        public void Initialize()
        {
            _sut = new CSVReaderWriterForAnnotation();
            _fileId = Guid.NewGuid();
            _currentDirectoryPath = TestContext.CurrentContext.TestDirectory;
        }

        #region Open

        [Test]
        public void Open_ForRead_FileNameNull_ShouldThrowException()
        {
            try
            {
                _sut.Open(null, FileOperationMode.Read);
                Assert.Fail("It should've thrown an exception.");
            }
            catch (Exception ex)
            {
                Assert.IsNotNull(ex);
                Assert.IsInstanceOf<ArgumentNullException>(ex);
            }
        }

        [Test]
        public void Open_ForRead_FileNameStringEmpty_ShouldThrowException()
        {
            try
            {
                _sut.Open(string.Empty, FileOperationMode.Read);
                Assert.Fail("It should've thrown an exception.");
            }
            catch (Exception ex)
            {
                Assert.IsNotNull(ex);
                Assert.IsInstanceOf<ArgumentException>(ex);
            }
        }

        [Test]
        public void Open_ForWrite_FileNameNull_ShouldThrowException()
        {
            try
            {
                _sut.Open(null, FileOperationMode.Write);
                Assert.Fail("It should've thrown an exception.");
            }
            catch (Exception ex)
            {
                Assert.IsNotNull(ex);
                Assert.IsInstanceOf<ArgumentNullException>(ex);
            }
        }

        [Test]
        public void Open_ForWrite_FileNameStringEmpty_ShouldThrowException()
        {
            try
            {
                _sut.Open(string.Empty, FileOperationMode.Write);
                Assert.Fail("It should've thrown an exception.");
            }
            catch (Exception ex)
            {
                Assert.IsNotNull(ex);
                Assert.IsInstanceOf<ArgumentException>(ex);
            }
        }

        [Test]
        public void Open_ForReadAndWrite_FileNameNull_ShouldThrowException()
        {
            try
            {
                _sut.Open(null, FileOperationMode.Read | FileOperationMode.Write);
                Assert.Fail("It should've thrown an exception.");
            }
            catch (Exception ex)
            {
                Assert.IsNotNull(ex);
            }
        }

        [Test]
        public void Open_ForReadAndWrite_FileNameStringEmpty_ShouldThrowException()
        {
            try
            {
                _sut.Open(string.Empty, FileOperationMode.Read | FileOperationMode.Write);
                Assert.Fail("It should've thrown an exception.");
            }
            catch (Exception ex)
            {
                Assert.IsNotNull(ex);
            }
        }

        [Test]
        public void Open_ForRead_FileDoesNotExist_ShouldThrowException()
        {
            try
            {
                _sut.Open($"{_currentDirectoryPath}\\does_not_exist.csv", FileOperationMode.Read);
                Assert.Fail("It should've thrown an exception.");
            }
            catch (Exception ex)
            {
                Assert.IsNotNull(ex);
                Assert.IsInstanceOf<FileNotFoundException>(ex);
            }
        }

        [Test]
        public void Open_ForRead_FileInWrongLocation_ShouldThrowException()
        {
            try
            {
                _sut.Open($"{_currentDirectoryPath}\\does_not_exist\\contacts.csv", FileOperationMode.Read);
                Assert.Fail("It should've thrown an exception.");
            }
            catch (Exception ex)
            {
                Assert.IsNotNull(ex);
                Assert.IsInstanceOf<DirectoryNotFoundException>(ex);
            }
        }

        [Test]
        public void Open_ForWrite_FileAlreadyExist_ShouldNotThrowException()
        {
            try
            {
                var filePath = $"{_currentDirectoryPath}\\test_data\\{_fileId}.csv";
                _sut.Open(filePath, FileOperationMode.Write);
                _sut.Close();
                _sut.Open(filePath, FileOperationMode.Write);
            }
            catch (Exception)
            {
                Assert.Fail("It should not have thrown an exception.");
            }
        }

        [Test]
        public void Open_ForRead_FileAlreadyExist_ShouldNotThrowException()
        {
            try
            {
                var filePath = $"{_currentDirectoryPath}\\test_data\\{_fileId}.csv";
                _sut.Open(filePath, FileOperationMode.Write);
                _sut.Close();
                _sut.Open(filePath, FileOperationMode.Read);
            }
            catch (Exception)
            {
                Assert.Fail("It should not have thrown an exception.");
            }
        }

        #endregion

        #region Close

        [Test]
        public void Close_OnNonOpenedFile_ShouldNotThrowException()
        {
            try
            {
                _sut.Close();
            }
            catch (Exception)
            {
                Assert.Fail("It should not have thrown an exception.");
            }
        }

        #endregion

        #region Read

        [Test]
        public void Read_FileExist_DeprecatedMethod_ShouldReturnNothing()
        {
            try
            {
                var filePath = $"{_currentDirectoryPath}\\test_data\\contacts.csv";
                _sut.Open(filePath, FileOperationMode.Read);

                string column1 = null, column2 = null;
                var shouldHaveBeenSuccess = _sut.Read(column1, column2);

                Assert.IsTrue(shouldHaveBeenSuccess);
                Assert.IsNull(column1);
                Assert.IsNull(column2);
            }
            catch (Exception)
            {
                Assert.Fail("It should not have thrown an exception.");
            }
        }

        [Test]
        public void Read_FileExist_ShouldReturnValues()
        {
            try
            {
                var filePath = $"{_currentDirectoryPath}\\test_data\\contacts.csv";
                _sut.Open(filePath, FileOperationMode.Read);

                var isSuccess = _sut.Read(out var column1, out var column2);

                Assert.IsTrue(isSuccess);
                Assert.IsNotNull(column1);
                Assert.IsNotNull(column2);
            }
            catch (Exception)
            {
                Assert.Fail("It should not have thrown an exception.");
            }
        }

        [Test]
        public void Read_FileExist_ShouldReturnConsecutiveValues()
        {
            try
            {
                var filePath = $"{_currentDirectoryPath}\\test_data\\contacts.csv";
                _sut.Open(filePath, FileOperationMode.Read);

                var isFirstReadSuccess = _sut.Read(out var row1Column1, out var row1Column2);
                Assert.IsTrue(isFirstReadSuccess);

                var isSuccess = _sut.Read(out var row2Column1, out var row2Column2);
                Assert.IsTrue(isSuccess);

                Assert.AreNotEqual(row1Column1, row2Column1);
                Assert.AreNotEqual(row1Column2, row2Column2);
            }
            catch (Exception)
            {
                Assert.Fail("It should not have thrown an exception.");
            }
        }

        [Test]
        public void Read_OpenForWrite_DeprecatedMethod_ShouldThrowException()
        {
            try
            {
                var filePath = $"{_currentDirectoryPath}\\test_data\\{_fileId}.csv";
                _sut.Open(filePath, FileOperationMode.Write);

                string column1 = null, column2 = null;
                _sut.Read(column1, column2);

                Assert.Fail("It should have thrown an exception.");
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOf<NullReferenceException>(ex);
            }
        }

        [Test]
        public void Read_OpenForWrite_ShouldThrowException()
        {
            try
            {
                var filePath = $"{_currentDirectoryPath}\\test_data\\{_fileId}.csv";
                _sut.Open(filePath, FileOperationMode.Write);

                _sut.Read(out var column1, out var column2);

                Assert.Fail("It should have thrown an exception.");
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOf<NullReferenceException>(ex);
            }
        }

        #endregion

        #region Write

        [Test]
        public void Write_FileDoesntExist_ShouldCreateFileAndWrite()
        {
            try
            {
                var filePath = $"{_currentDirectoryPath}\\test_data\\{_fileId}.csv";
                _sut.Open(filePath, FileOperationMode.Write);

                _sut.Write("test 0", "test 1");
            }
            catch (Exception)
            {
                Assert.Fail("It should not have thrown an exception.");
            }
        }

        [Test]
        public void Write_FileExist_ShouldResetContents()
        {
            try
            {
                var filePath = $"{_currentDirectoryPath}\\test_data\\{_fileId}.csv";

                _sut.Open(filePath, FileOperationMode.Write);
                _sut.Write("test 0", "test 1");
                _sut.Close();

                _sut.Open(filePath, FileOperationMode.Write);
                _sut.Write("test 2", "test 3");
                _sut.Close();

                _sut.Open(filePath, FileOperationMode.Read);
                _sut.Read(out var column1, out var column2);

                Assert.AreEqual("test 2", column1);
                Assert.AreEqual("test 3", column2);
            }
            catch (Exception)
            {
                Assert.Fail("It should not have thrown an exception.");
            }
        }

        [Test]
        public void Write_MultipleEntries_ShouldSaveAll()
        {
            try
            {
                var filePath = $"{_currentDirectoryPath}\\test_data\\{_fileId}.csv";

                _sut.Open(filePath, FileOperationMode.Write);
                _sut.Write("test 0", "test 1");
                _sut.Write("test 2", "test 3");
                _sut.Write("test 4", "test 5");
                _sut.Write("test 6", "test 7");
                _sut.Close();

                _sut.Open(filePath, FileOperationMode.Read);
                _sut.Read(out var row1Column1, out var row1Column2);
                _sut.Read(out var row2Column1, out var row2Column2);
                _sut.Read(out var row3Column1, out var row3Column2);
                _sut.Read(out var row4Column1, out var row4Column2);

                Assert.AreEqual("test 0", row1Column1);
                Assert.AreEqual("test 1", row1Column2);
                Assert.AreEqual("test 2", row2Column1);
                Assert.AreEqual("test 3", row2Column2);
                Assert.AreEqual("test 4", row3Column1);
                Assert.AreEqual("test 5", row3Column2);
                Assert.AreEqual("test 6", row4Column1);
                Assert.AreEqual("test 7", row4Column2);
            }
            catch (Exception)
            {
                Assert.Fail("It should not have thrown an exception.");
            }
        }

        [Test]
        public void Write_OpenForRead_DeprecatedMethod_ShouldThrowException()
        {
            try
            {
                var filePath = $"{_currentDirectoryPath}\\test_data\\contacts.csv";
                _sut.Open(filePath, FileOperationMode.Read);

                _sut.Write("test 0", "test 1");

                Assert.Fail("It should have thrown an exception.");
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOf<NullReferenceException>(ex);
            }
        }

        [Test]
        public void Write_OpenForRead_ShouldThrowException()
        {
            try
            {
                var filePath = $"{_currentDirectoryPath}\\test_data\\contacts.csv";
                _sut.Open(filePath, FileOperationMode.Read);

                _sut.Write("test 0", "test 1");

                Assert.Fail("It should have thrown an exception.");
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOf<NullReferenceException>(ex);
            }
        }

        #endregion
    }
}
